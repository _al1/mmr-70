/*
 * playing some music. 
 * Written by al1 
 */ 
#include <avr/io.h>
#include <avr/interrupt.h>
#include "MUSIC_tiny.h"

static volatile uint16_t Array [21] [2] = {		//some Wagner
	{185,16},									// tone [Hz], duration [ticks] the higher the longer the tone 
	{247,24},
	{185,8},
	{247,16},
	{294,48},
	{247,48},
	
	{294,24},
	{247,8},
	{294,16},
	{370,48},
	{294,48},
	
	{370,24},
	{294,8},
	{370,16},
	{440,48},
	{220,48},
	
	{294,24},
	{247,8},
	{294,16},
	{370,96},
	
	{0,100}
};

static volatile uint8_t toneT = 0;
static volatile uint8_t indexArray=0;
static const uint8_t maxIndex =20;

static inline void timerOC0_off (void)
{
	TCCR2 &= ~(1<<COM20);	//disconnect 0C2
	PORTD |= (1<<PD7);		
}

static inline void timerOC0_on (void)
{
	TCCR2 |= (1<<COM20);	//connect 0C1A
}

static inline void setOCR2AValue(uint16_t freq)
{
	OCR2 = (uint8_t)(((uint16_t) 28800)/freq)-1; // F_CPU / ( freq * 2 * preScaler)
}

ISR(TIMER0_COMP_vect)		//TIMER 0 used for timing and length
{
	if (toneT==0)
	{
		toneT=Array[indexArray][1];
			
		if (Array[indexArray][0])
		timerOC0_on();
		else
		timerOC0_off();
		setOCR2AValue(Array[indexArray][0]);	// Timer 2 used for tone playback
			
		indexArray++;
		if (indexArray>maxIndex)
		{
			indexArray=0;
			TIMSK &= ~(1<<OCF0);		//music off
			
		}			
	}
	if (toneT<3)
	timerOC0_off();
	toneT--;
}
	
void MUSIC_setup(void)
{
	DDRD  |= (1<<PD7);						//OC2 output
	TCCR2 |= /*(1<<COM1A0) | */(1<<WGM21);	//toggle 0C1A on compare match AND CTC mode
	TCCR2 |= (1<<CS22);						//prescaler = 64
	TCCR0 |= (1<<WGM01);					//CTC Mode
	TCCR0 |= (1<<CS02)/*|(1<<CS00)*/;		//prescaler = 256
	OCR0   = 150;							//set playback speed with this
	//TIMSK |= (1<<OCF0A);
}

void MUSIC_play(void)
{
	//TCNT0=/*(OCR0/ *-1* /)*/149;
	TIMSK |= (1<<OCF0);
}