
#define F_CPU 3688400UL

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	DDRD |=  (1<<PD7);		
	PORTD &= ~(1<<PD7);		// Led off
	
    while(1)
    {
        PORTD ^= (1<<PD7);
		_delay_ms(200);
		
    }
}
